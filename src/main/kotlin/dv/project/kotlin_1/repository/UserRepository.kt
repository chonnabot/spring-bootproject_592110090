package dv.project.kotlin_1.repository

import dv.project.kotlin_1.entity.User
import dv.project.kotlin_1.entity.Wallet
import org.springframework.data.repository.CrudRepository

interface UserRepository: CrudRepository<User, Long> {
//     fun findWalletsByWallets_IsDeletedIsFalse(): List<User>
}
