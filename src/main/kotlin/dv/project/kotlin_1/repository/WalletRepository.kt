package dv.project.kotlin_1.repository

import dv.project.kotlin_1.entity.Wallet
import org.springframework.data.repository.CrudRepository

interface WalletRepository: CrudRepository<Wallet, Long> {
    fun findByIsDeletedIsFalse(): List<Wallet>
//    fun findWalletByIsDeletedIsFalse(): Wallet?
//    fun findByIdIsIsDeletedIsFalse(): Wallet
}