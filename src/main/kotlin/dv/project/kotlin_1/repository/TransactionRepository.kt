package dv.project.kotlin_1.repository

import dv.project.kotlin_1.entity.Transaction
import org.springframework.data.repository.CrudRepository

interface TransactionRepository : CrudRepository<Transaction, Long>{

}