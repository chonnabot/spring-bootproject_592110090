package dv.project.kotlin_1.dao

import dv.project.kotlin_1.entity.Wallet

interface WalletDao {
    fun getAllwallets(): List<Wallet>
    fun save(wallet: Wallet): Wallet
    fun getWalletById(id: Long): Wallet?
    fun findById(id: Long?): Wallet

}