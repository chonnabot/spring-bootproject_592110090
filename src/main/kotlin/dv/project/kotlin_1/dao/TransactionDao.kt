package dv.project.kotlin_1.dao

import dv.project.kotlin_1.entity.Transaction

interface TransactionDao {
    fun getAlltransactions(): List<Transaction>
    fun save(transaction: Transaction): Transaction
    fun getTransactionById(id: Long): Transaction?
}
