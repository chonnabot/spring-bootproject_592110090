package dv.project.kotlin_1.dao

import dv.project.kotlin_1.entity.User

interface UserDao {
    fun getAllUsers(): List<User>
    fun getUserById(id: Long): User?
    fun save(user: User): User
    fun findById(id: Long): User
    fun saveRegister(userRegister: User): User
}