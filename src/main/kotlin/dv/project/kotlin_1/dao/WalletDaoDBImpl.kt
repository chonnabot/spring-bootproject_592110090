package dv.project.kotlin_1.dao

import dv.project.kotlin_1.entity.Wallet
import dv.project.kotlin_1.repository.WalletRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository


@Profile("db")
@Repository
class WalletDaoDBImpl : WalletDao {
    override fun findById(id: Long?): Wallet {
        return walletRepository.findById(id!!).orElse(null)
    }

    override fun getWalletById(id: Long): Wallet? {
        return walletRepository.findById(id).orElse(null)
//       return walletRepository.findWalletByIsDeletedIsFalse()
    }

    override fun save(wallet: Wallet): Wallet {
        return walletRepository.save(wallet)
    }

    override fun getAllwallets(): List<Wallet> {
//        return walletRepository.findAll().filterIsInstance(Wallet::class.java)
        return walletRepository.findByIsDeletedIsFalse()
    }

    @Autowired
    lateinit var walletRepository: WalletRepository
}