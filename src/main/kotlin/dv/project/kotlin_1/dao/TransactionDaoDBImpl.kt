package dv.project.kotlin_1.dao

import dv.project.kotlin_1.entity.Transaction
import dv.project.kotlin_1.repository.TransactionRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository

@Profile("db")
@Repository
class TransactionDaoDBImpl :  TransactionDao{

    override fun getTransactionById(id: Long): Transaction? {
        return transactionRepository.findById(id).orElse(null)
    }


    override fun save(transaction: Transaction): Transaction {
        return transactionRepository.save(transaction)
    }

    override fun getAlltransactions(): List<Transaction> {
        return transactionRepository.findAll().filterIsInstance(Transaction::class.java)
    }

    @Autowired
    lateinit var transactionRepository: TransactionRepository



}
