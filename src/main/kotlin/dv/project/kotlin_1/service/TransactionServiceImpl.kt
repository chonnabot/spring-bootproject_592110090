package dv.project.kotlin_1.service

import dv.project.kotlin_1.dao.TransactionDao
import dv.project.kotlin_1.dao.UserDao
import dv.project.kotlin_1.dao.WalletDao
import dv.project.kotlin_1.entity.Transaction
import dv.project.kotlin_1.entity.Wallet
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.lang.RuntimeException
import javax.transaction.Transactional

@Service
class TransactionServiceImpl : TransactionService {

    override fun remove(transactionId: Long): Transaction? {
        val transaction = transactionDao.getTransactionById(transactionId)
        transaction?.isDeleted = true
        return transaction
    }

    @Transactional
    override fun save(walletId: Long, transaction: Transaction): Transaction {
        val wallet = walletDao.getWalletById(walletId)
        if (wallet != null) {
            val newTransaction = transactionDao.save(transaction)
            wallet.transactions?.add(newTransaction)
            println(wallet.toString())
            return newTransaction
        } else {
            throw RuntimeException("Not found walletId")
        }

    }

    @Transactional
    override fun getAllTransactions(): List<Transaction> {
        return transactionDao.getAlltransactions()
    }

    @Autowired
    lateinit var userDao: UserDao

    @Autowired
    lateinit var transactionDao: TransactionDao

    @Autowired
    lateinit var walletDao: WalletDao


}
