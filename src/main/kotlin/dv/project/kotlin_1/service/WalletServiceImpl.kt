package dv.project.kotlin_1.service

import dv.project.kotlin_1.dao.UserDao
import dv.project.kotlin_1.dao.WalletDao
import dv.project.kotlin_1.entity.Wallet
import dv.project.kotlin_1.entity.dto.EditWalletDto
import dv.project.kotlin_1.entity.dto.WalletDto
import dv.project.kotlin_1.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.lang.RuntimeException
import javax.transaction.Transactional

@Service
class WalletServiceImpl : WalletService {
    override fun getWalletbyWalletId(id: Long): Wallet? {
        return walletDao.getWalletById(id)
    }

    override fun saveNewNameWallet(walletId: Long, editWalletDto: EditWalletDto): Wallet {
        val wallet = walletDao.findById(walletId)
        wallet.walletName = editWalletDto.walletName
        return walletDao.save(wallet)
    }

    @Transactional
    override fun remove(walletId: Long): Wallet? {
        val wallet = walletDao.getWalletById(walletId)
        wallet?.isDeleted = true
        return wallet
    }

    @Transactional
    override fun save(userId: Long, wallet: Wallet): Wallet {
        val user = userDao.getUserById(userId)
        if (user != null) {
            val newWallet = walletDao.save(wallet)
            user?.wallets?.add(newWallet)
            return newWallet
        } else {
            throw RuntimeException("Not found WalletId")
        }
    }

    @Transactional
    override fun getAllWallets(): List<Wallet> {
        return walletDao.getAllwallets()
    }



    @Autowired
    lateinit var userDao: UserDao

    @Autowired
    lateinit var walletDao: WalletDao
}