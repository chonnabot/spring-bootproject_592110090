package dv.project.kotlin_1.service

import dv.project.kotlin_1.entity.Transaction

interface TransactionService {
    fun getAllTransactions(): List<Transaction>
    fun save(walletId: Long, transaction: Transaction): Transaction
    fun remove(transactionId: Long): Transaction?

}