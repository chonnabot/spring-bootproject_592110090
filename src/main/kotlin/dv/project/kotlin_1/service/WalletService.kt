package dv.project.kotlin_1.service

import dv.project.kotlin_1.entity.Wallet
import dv.project.kotlin_1.entity.dto.EditWalletDto

interface WalletService{
    fun getAllWallets(): List<Wallet>
    fun save(userId: Long, wallet: Wallet): Wallet
    fun remove(walletId: Long): Wallet?
    fun saveNewNameWallet(walletId: Long, editWalletDto: EditWalletDto): Wallet
    fun getWalletbyWalletId(id: Long): Wallet?

}