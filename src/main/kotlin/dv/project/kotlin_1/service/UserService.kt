package dv.project.kotlin_1.service

import dv.project.kotlin_1.entity.User
import dv.project.kotlin_1.entity.dto.EditUserDto

interface UserService {
    fun getAllUsers(): List<User>
    fun save(user: User): User
    fun getUserbyUserId(id: Long): User?
    fun saveEditUser(userId: Long, editUserDto: EditUserDto): User
    fun saveRegister(userRegisterDto: User): User
}