package dv.project.kotlin_1.service

import dv.project.kotlin_1.dao.UserDao
import dv.project.kotlin_1.entity.User
import dv.project.kotlin_1.entity.dto.EditUserDto
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import javax.transaction.Transactional

@Service
class UserServiceImpl : UserService {
    override fun saveRegister(userRegisterDto: User): User {
        return userDao.saveRegister(userRegisterDto)
    }

    override fun saveEditUser(userId: Long, editUserDto: EditUserDto): User {
        val user = userDao.findById(userId)
        user.firstName = editUserDto.firstName
        user.lastName = editUserDto.lastName
        user.password = editUserDto.password
        user.userImage = editUserDto.userImage
        return userDao.save(user)
    }

    override fun getUserbyUserId(id: Long): User? {
        return userDao.getUserById(id)
    }

    @Transactional
    override fun save(user: User): User {
        return userDao.save(user)
    }

    override fun getAllUsers(): List<User> {
        return userDao.getAllUsers()
    }

    @Autowired
    lateinit var userDao: UserDao


}
