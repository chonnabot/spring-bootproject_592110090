package dv.project.kotlin_1.util

import dv.project.kotlin_1.entity.Transaction
import dv.project.kotlin_1.entity.User
import dv.project.kotlin_1.entity.Wallet
import dv.project.kotlin_1.entity.dto.*
import dv.project.kotlin_1.security.entity.Authority
import org.mapstruct.InheritInverseConfiguration
import org.mapstruct.Mapper
import org.mapstruct.Mapping
import org.mapstruct.Mappings
import org.mapstruct.factory.Mappers
import java.beans.Customizer

@Mapper(componentModel = "spring")
interface MapperUtil {
    companion object {
        val INSTANCE = Mappers.getMapper(MapperUtil::class.java)
    }

    fun mapUserDto(user: User): UserDto
    fun mapUserDto(user: List<User>): List<UserDto>
    @InheritInverseConfiguration
    fun mapUserDto(userDto: UserDto): User

    fun mapWalletDto(wallet: Wallet?): WalletDto?
    fun mapWalletDto(wallet: List<Wallet>): List<WalletDto>
    @InheritInverseConfiguration
    fun mapWalletDto(walletDto: WalletDto): Wallet

    fun mapEditWalletDto(wallet: Wallet): EditWalletDto


    fun mapTransactionDto(transaction: Transaction?): TransactionDto?
    fun mapTransactionDto(transaction: List<Transaction>): List<TransactionDto>
    @InheritInverseConfiguration
    fun mapTransactionDto(transactionDto: TransactionDto): Transaction

    fun mapUserAccountDto(user: User): UserAccountDto

    fun mapAuthority(authority: Authority): AuthorityDto
    fun mapAuthority(authority: List<Authority>): List<AuthorityDto>

    fun mapUserRegisterDto(user: User):UserRegisterDto
    @InheritInverseConfiguration
    fun mapUserRegisterDto(userRegisterDto: UserRegisterDto): User


}