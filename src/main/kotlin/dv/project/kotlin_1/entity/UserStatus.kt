package dv.project.kotlin_1.entity

enum class UserStatus{
    ACTIVE,DELETED,PENDING
}