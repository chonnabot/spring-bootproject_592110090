package dv.project.kotlin_1.entity

import java.time.LocalDate
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id

@Entity
data class Transaction(
        var date: LocalDate? = null,
        var transactionStatus: TransactionStatus? = null,
        var reference: String? = null,
        var money: Double? = null,
        var isDeleted: Boolean? = false
){
    @Id
    @GeneratedValue
    var id:Long? = null

}
