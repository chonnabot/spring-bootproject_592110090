package dv.project.kotlin_1.entity

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.OneToMany

@Entity
data class User(override var firstName: String? = null,
                override var lastName: String? = null,
                override var email: String? = null,
                override var password: String? = null,
                override var userImage: String? = null,
                override var userStatus: UserStatus? = UserStatus.PENDING)
    : UserAccount(firstName,
        lastName,
        email,
        password,
        userImage,
        userStatus) {

    @OneToMany
    var wallets = mutableListOf<Wallet>()
}