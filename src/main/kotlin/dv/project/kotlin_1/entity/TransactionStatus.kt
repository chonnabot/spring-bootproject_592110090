package dv.project.kotlin_1.entity

enum class TransactionStatus {
    REVENUE, EXPENSES

}