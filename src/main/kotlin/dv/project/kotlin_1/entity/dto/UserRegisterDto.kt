package dv.project.kotlin_1.entity.dto

data class UserRegisterDto(
        var firstName: String? = null,
        var lastName: String? = null,
        var email: String? = null)