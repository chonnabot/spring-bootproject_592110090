package dv.project.kotlin_1.entity.dto

data class UserAccountDto(
        var firstName: String? = null,
        var lastName: String? = null,
        var email: String? = null)
