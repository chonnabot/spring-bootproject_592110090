package dv.project.kotlin_1.entity.dto

data class UserDto(
                   var id: Long? = null,
                   var firstName: String? = null,
                   var lastName: String? = null,
                   var email: String? = null,
                   var password: String? = null,
                   var userImage: String? = null,
                   var wallets: List<WalletDto>? = emptyList())
