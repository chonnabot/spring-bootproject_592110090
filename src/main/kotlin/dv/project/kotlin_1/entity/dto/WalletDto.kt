package dv.project.kotlin_1.entity.dto

data class WalletDto(var walletName: String? = null,
                     var money: Double? = null,
                     var id: Long? =null,
                     var isDeleted: Boolean? = false,
                     var transactions: List<TransactionDto>? = emptyList())
