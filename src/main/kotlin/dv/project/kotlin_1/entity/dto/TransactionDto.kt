package dv.project.kotlin_1.entity.dto

import dv.project.kotlin_1.entity.TransactionStatus
import java.time.LocalDate

data class TransactionDto(var date: LocalDate? = null,
                          var transactionStatus: TransactionStatus? = null,
                          var reference: String? = null,
                          var money: Double? = null,
                          var id: Long? =null,
                          var isDeleted: Boolean? = false
)