package dv.project.kotlin_1.entity.dto

data class EditWalletDto(var walletName: String? = null,
                         var id: Long? =null)