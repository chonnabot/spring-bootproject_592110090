package dv.project.kotlin_1.entity.dto

data class EditUserDto(
        var firstName: String? = null,
        var lastName: String? = null,
        var password: String? = null,
        var userImage: String? = null
)