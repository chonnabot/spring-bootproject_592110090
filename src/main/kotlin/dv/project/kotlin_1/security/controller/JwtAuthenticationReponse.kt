package dv.project.kotlin_1.security.controller

data class JwtAuthenticationResponse(
        var token: String? = null
)