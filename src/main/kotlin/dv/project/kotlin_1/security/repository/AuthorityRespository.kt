package dv.project.kotlin_1.security.repository

import dv.project.kotlin_1.security.entity.Authority
import dv.project.kotlin_1.security.entity.AuthorityName
import org.springframework.data.repository.CrudRepository

interface AuthorityRepository: CrudRepository<Authority, Long> {
    fun findByName(input: AuthorityName): Authority
}