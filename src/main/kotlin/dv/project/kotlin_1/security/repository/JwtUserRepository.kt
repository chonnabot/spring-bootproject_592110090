package dv.project.kotlin_1.security.repository

import dv.project.kotlin_1.security.entity.JwtUser
import org.springframework.data.repository.CrudRepository

interface JwtUserRepository: CrudRepository<JwtUser, Long> {
    fun findByUsername(username: String): JwtUser
}