package dv.project.kotlin_1.config

import dv.project.kotlin_1.entity.Transaction
import dv.project.kotlin_1.entity.TransactionStatus
import dv.project.kotlin_1.entity.User
import dv.project.kotlin_1.entity.Wallet
import dv.project.kotlin_1.repository.TransactionRepository
import dv.project.kotlin_1.repository.UserRepository

import dv.project.kotlin_1.repository.WalletRepository
import dv.project.kotlin_1.security.entity.Authority
import dv.project.kotlin_1.security.entity.AuthorityName
import dv.project.kotlin_1.security.entity.JwtUser
import dv.project.kotlin_1.security.repository.AuthorityRepository
import dv.project.kotlin_1.security.repository.JwtUserRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.ApplicationArguments
import org.springframework.boot.ApplicationRunner
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.stereotype.Component
import java.time.LocalDate
import javax.transaction.Transactional

@Component
class ApplicationLoader : ApplicationRunner {
    @Transactional
    fun loadUsernameAndPassword() {
        val auth1 = Authority(name = AuthorityName.ROLE_ADMIN)
        val auth2 = Authority(name = AuthorityName.ROLE_CUSTOMER)
        val auth3 = Authority(name = AuthorityName.ROLE_GENERAL)
        authorityRepository.save(auth1)
        authorityRepository.save(auth2)
        authorityRepository.save(auth3)
        val encoder = BCryptPasswordEncoder()
        val userl = User("Stamp", "Chonnabot", "tamlks@hotmail.com", "23400949", "https://scontent-kut2-2.xx.fbcdn.net/v/t1.0-9/12509577_1015363731856842_3365828373799268203_n.jpg?_nc_cat=109&_nc_ht=scontent-kut2-2.xx&oh=07b270db3192329ccab30a0985b3ecf8&oe=5D030E7A")
        val userJwt = JwtUser(
                username = "User",
                password = encoder.encode("password"),
                email = userl.email,
                enabled = true,
                firstname = userl.firstName,
                lastname = userl.lastName
        )
        userRepository.save(userl)
        jwtUserRepository.save(userJwt)
        userl.jwtUser = userJwt
        userJwt.user = userl
        userJwt.authorities.add(auth2)
        userJwt.authorities.add(auth3)


    }

    @Transactional
    override fun run(args: ApplicationArguments?) {

        var user1 = userRepository.save(
                User("Stamp", "Chonnabot", "tamlks@hotmail.com", "23400949", "https://scontent-kut2-2.xx.fbcdn.net/v/t1.0-9/12509577_1015363731856842_3365828373799268203_n.jpg?_nc_cat=109&_nc_ht=scontent-kut2-2.xx&oh=07b270db3192329ccab30a0985b3ecf8&oe=5D030E7A")
        )

        var wallet1 = walletRepository.save(
                Wallet("testWallet", 0.00, false)
        )

        var transaction1 = transactionReposity.save(
                Transaction(LocalDate.parse("2019-03-23"), TransactionStatus.REVENUE, "From mom", 1000.00)

        )

        user1.wallets.add(wallet1)

        wallet1.transactions.add(transaction1)

        loadUsernameAndPassword()
//aa
    }

    @Autowired
    lateinit var userRepository: UserRepository

    @Autowired
    lateinit var authorityRepository: AuthorityRepository

    @Autowired
    lateinit var jwtUserRepository: JwtUserRepository

    @Autowired
    lateinit var walletRepository: WalletRepository

    @Autowired
    lateinit var transactionReposity: TransactionRepository


}