package dv.project.kotlin_1.controller

import dv.project.kotlin_1.entity.User
import dv.project.kotlin_1.entity.dto.EditUserDto
import dv.project.kotlin_1.entity.dto.UserDto
import dv.project.kotlin_1.service.UserService
import dv.project.kotlin_1.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
class UserController {
    @Autowired
    lateinit var userService: UserService

    @GetMapping("/users")
    fun getAllUser(): ResponseEntity<Any> {
        val output = userService.getAllUsers()
        return ResponseEntity.ok(
                MapperUtil.INSTANCE.mapUserDto(output))
    }

    @PostMapping("/users/register")
    fun register(@RequestBody user: User): ResponseEntity<Any> {
        val output = userService.saveRegister(user)
        val outputDto = MapperUtil.INSTANCE.mapUserRegisterDto(output)
        return ResponseEntity.ok(outputDto)
    }


    @PostMapping("/users/add/")
    fun addUser(
            @RequestBody user: UserDto): ResponseEntity<Any> {
        val output = userService.save(MapperUtil.INSTANCE.mapUserDto(user))
        val outputDto = MapperUtil.INSTANCE.mapUserDto(output)
        outputDto?.let { return ResponseEntity.ok(it) }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).build()
    }

    @PutMapping("/user/editUser/{userId}")
    fun editUser(@PathVariable("userId") userId: Long,
                 @RequestBody editUserDto: EditUserDto): ResponseEntity<Any> {
        try {
            val output = userService.saveEditUser(userId, editUserDto)
            return ResponseEntity.ok(output)
        } catch (e: RuntimeException) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("not found userId")
        }
    }

    @GetMapping("/users/usersId/{id}")
    fun getUserbyUserId(@PathVariable("id") id: Long)
            : ResponseEntity<Any> {
        val output = userService.getUserbyUserId(id)
        output?.let { return ResponseEntity.ok(output) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body("not found usersId")
    }

}
