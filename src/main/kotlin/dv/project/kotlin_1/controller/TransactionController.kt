package dv.project.kotlin_1.controller

import dv.project.kotlin_1.entity.Transaction
import dv.project.kotlin_1.entity.dto.TransactionDto
import dv.project.kotlin_1.service.TransactionService
import dv.project.kotlin_1.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.lang.RuntimeException

@RestController
class TransactionController {

    @Autowired
    lateinit var transactionService: TransactionService

    @GetMapping("/transactions")
    fun getAllTransactions(): ResponseEntity<Any> {
        val output = transactionService.getAllTransactions()
        return ResponseEntity.ok(
                MapperUtil.INSTANCE.mapTransactionDto(output))
    }

    @PostMapping("/transaction/walletId/{walletId}")
    fun addTransaction(
            @PathVariable("walletId") walletId: Long,
            @RequestBody transaction: Transaction): ResponseEntity<Any> {
        try {
            val output = transactionService.save(walletId, transaction)
            val outputDto = MapperUtil.INSTANCE.mapTransactionDto(output)
            outputDto?.let { return ResponseEntity.ok(it) }
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build()
        } catch (e: RuntimeException) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("not found WalletId")
        }
    }

    @DeleteMapping("/transaction/id/{transactionId}")
    fun deleteTransaction(@PathVariable("transactionId") transactionId: Long): ResponseEntity<Any> {
        val transaction = transactionService.remove(transactionId)
        val outputProduct = MapperUtil.INSTANCE.mapTransactionDto(transaction)
        outputProduct?.let { return ResponseEntity.ok(it) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body("not found transactionId")
    }

//    @PostMapping("/transaction/walletId/{walletId}")
//    fun addTransactionToWallet(@PathVariable("walletId") walletId: Long,
//                               @RequestBody transaction: TransactionDto): ResponseEntity<Any> {
//        val output =
//
//    }


}

