package dv.project.kotlin_1.controller

import dv.project.kotlin_1.entity.dto.EditWalletDto
import dv.project.kotlin_1.entity.dto.WalletDto
import dv.project.kotlin_1.service.WalletService
import dv.project.kotlin_1.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.lang.RuntimeException

@RestController
class WalletController {
    @Autowired
    lateinit var walletService: WalletService

    @GetMapping("/wallets")
    fun getAllWallets(): ResponseEntity<Any> {
        val output = walletService.getAllWallets()
        return ResponseEntity.ok(
                output)
    }

    @GetMapping("/wallets/walletId/{id}")
    fun getWalletbyUserId(@PathVariable("id") id: Long)
            : ResponseEntity<Any> {
        val output = walletService.getWalletbyWalletId(id)
        output?.let {return ResponseEntity.ok(output)}
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body("not found walletId")
    }


    @PostMapping("/wallets/userId/{userId}")
    fun addWallet(@PathVariable("userId") userId: Long,
                  @RequestBody wallet: WalletDto)
            : ResponseEntity<Any> {
        try {
            val output = walletService.save(userId, MapperUtil.INSTANCE.mapWalletDto(wallet))
            val outputDto = MapperUtil.INSTANCE.mapWalletDto(output)
            outputDto?.let { return ResponseEntity.ok(it) }
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build()
        } catch (e: RuntimeException) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("not found userId")
        }
    }

    @DeleteMapping("/wallets/id/{walletId}")
    fun deleteWallet(@PathVariable("walletId") walletId: Long): ResponseEntity<Any> {
        val wallet = walletService.remove(walletId)
        val outputProduct = MapperUtil.INSTANCE.mapWalletDto(wallet)
        outputProduct?.let { return ResponseEntity.ok(it) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body("not found walletId")
    }

    @PutMapping("/wallets/editWallets/{editWallets}")
    fun editWallets(@PathVariable("editWallets") walletId: Long,
                    @RequestBody editWalletDto: EditWalletDto): ResponseEntity<Any> {
        try {
            val output = walletService.saveNewNameWallet(walletId, editWalletDto)
            return ResponseEntity.ok(output)
        } catch (e: RuntimeException) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("not found walletId")
        }
    }


}

